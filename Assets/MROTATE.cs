﻿using System.Collections;
using UnityEngine;

public class MROTATE : MonoBehaviour
{
    public float _distance = 5f;//距離中心的距離
    public float _angle = 10f;//轉幾度  轉速

    bool _isStart = false;

    Transform _center;
    Transform _surround;

    void Start()
    {
        _center = transform;
        _surround = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
        _center.name = "_center";
        _surround.name = "_surround";

        _surround.position = Vector3.Normalize(Vector3.forward) * _distance + _center.position;
        //StartCoroutine(InitEnd());
    }

    IEnumerator InitEnd()
    {
        yield return new WaitForSeconds(1);
        _isStart = true;
    }

    void Update()
    {
        //if (!_isStart) return;

        //绕Y轴旋转_angle度
        Quaternion rotate = Quaternion.AngleAxis(_angle, Vector3.up);
        //由中心指向圆周的方向向量
        Vector3 dir = Vector3.Normalize(_surround.position - _center.position);//圓周的點到圓心
        //向量OX = 向量OA + 向量AX
        _surround.position = _center.position  + (rotate * dir * _distance);
      
    }
}
