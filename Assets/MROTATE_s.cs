﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MROTATE_s : MonoBehaviour
{
    public float _distance = 1f;
    public float _speed = 40;
    public float _angle = 2f;

    bool _isStart = false;

    Transform _center;
    Transform _surround;
    float _value1 = 1;

    void Awake()
    {
        print("值"+_value1);
        _center = transform;
        _surround = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
        _center.name = "_center";
        _surround.name = "_surround";

        _surround.position = Vector3.Normalize(Vector3.forward) * _distance + _center.position;
        //这一步协程是为了避免螺纹线初始位置绘制错误
        StartCoroutine(InitEnd());
    }

    IEnumerator InitEnd()
    {
        yield return new WaitForSeconds(1);
        _isStart = true;
    }

    void Update()
    {
        print("值"+_value1);
       // if (!_isStart) return;
        //半径逐渐變大
        _value1 += _speed * Time.deltaTime ;
        _speed-=_speed *Time.deltaTime;
        float distance = _value1;
        //float distance = Mathf.Lerp(_distance, 10, _value1);
       // Mathf.Lerp(0, _distance, _value)
        print("距離"+distance+"速度"+_speed);
        //圆周运动
        Quaternion rotate = Quaternion.AngleAxis(_angle, Vector3.up);
        Vector3 dir = Vector3.Normalize(_surround.position - _center.position);
        _surround.position = rotate * dir * distance + _center.position;

        _pos.Add(_surround.position);
    }

    List<Vector3> _pos = new List<Vector3>();

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        //在Scene视图中绘制螺纹线
        for (int i = 0; i < _pos.Count - 1; i++)
        {
            Vector3 from = _pos[i];
            Vector3 to = _pos[i + 1];
            Gizmos.DrawLine(from, to);
        }
    }
}