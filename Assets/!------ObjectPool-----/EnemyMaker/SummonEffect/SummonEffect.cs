﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonEffect : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        summonEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<SummonEffectPool>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToEffectPool();
    }

    [SerializeField]
    private SummonEffectPool summonEffectPool;
    private float _timer = 0;
    public float lifeTime = 3.0f;

    private void OnEnable()
    {
        _timer = Time.time;
    }

    public void ReturnToEffectPool()
    {
        if (!gameObject.activeInHierarchy) { return; }

        if (Time.time > _timer + lifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            summonEffectPool.Recovery(this.gameObject);
        }
    }
}
