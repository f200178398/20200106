﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LaserMaker
{
    public class SkyLaserHitEffect : MonoBehaviour
    {
        [SerializeField]
        private SkyLaserHitEffectPool skyLaserHitEffectPool;
        private float _timer=0;
        public float lifeTime = 1.0f;

        private void Awake()
        {
            skyLaserHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<SkyLaserHitEffectPool>();
        }


        // Start is called before the first frame update
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
            HitEffectReturnToPool();
        }

        private void OnEnable()
        {
            _timer = Time.time;
        }

        public void HitEffectReturnToPool()
        {
            if (!gameObject.activeInHierarchy) { return; }

            if (Time.time > _timer + lifeTime)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
            {
                skyLaserHitEffectPool.Recovery(this.gameObject);
            }

        }
        


    }
}