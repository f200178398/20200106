﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
namespace LaserMaker
{
    public class Laser : MonoBehaviour
    {
        [SerializeField]
        private float f_laserTimer;
        [Tooltip("雷射要射多長")]
        public float f_howLongWillLaserShoot;

        public GameObject go_startLaser;
        public GameObject go_mainLaser;
        public GameObject go_endLaser;
        public FightSystem fightSystem;
        public GameObject go_LaserParant;
        //傷害判定
        public Ray ray_skyToGround;
        private RaycastHit hitObject;
        public bool b_laserDamageCounting = false;
        public Collider co_laserCollider;
        [HideInInspector]public float player_currentHP;
        public LaserColliderPool laserColliderPool;
        public float f_attackRate=0.2f;
        [SerializeField] float collider_timer = 0;

        //命中特效
        public SkyLaserHitEffectPool skyLaserHitEffectPool;
        public LaserHitGroundEffectPool laserHitGroundEffectPool;
        public float f_hitGroundTimer;
        public float f_hitGroundEffectMake = 0.25f;
        //回收至物件池
        public LaserPool laserPool;

        // Start is called before the first frame update
        private void Awake()
        {
            fightSystem = GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();
            co_laserCollider = GetComponent<Collider>();
            skyLaserHitEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<SkyLaserHitEffectPool>();
            laserPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserPool>();
            go_LaserParant = this.gameObject;
            laserHitGroundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserHitGroundEffectPool>();
            laserColliderPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<LaserColliderPool>();
        }

        void Start()
        {
            
        }

        private void OnEnable()
        {
            f_laserTimer = Time.time;
            go_startLaser.SetActive(false);
            go_mainLaser.SetActive(false);
            go_endLaser.SetActive(false);

            //傷害判定
            //ray_skyToGround = new Ray( go_LaserParant.transform.position ,go_LaserParant.transform.up);
            //fightSystem= GameObject.FindGameObjectWithTag("FightSystem").GetComponent<FightSystem>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            ShootLaser();
            ShootLaserCollider();
        }

        public void ShootLaser()
        {
            LaserDamageAndHitEffect();

            go_startLaser.SetActive(true);
            if (Time.time > f_laserTimer + 1.2f)//開始傷害判定
            {
                b_laserDamageCounting = true;
                
            }
            if (Time.time > f_laserTimer + 1.4f)
            {
                go_mainLaser.SetActive(true);
            }
            if (Time.time > f_laserTimer + 1.4f + f_howLongWillLaserShoot)
            {
                b_laserDamageCounting = false;
                go_mainLaser.SetActive(false);
                go_endLaser.SetActive(true);
            }
            if (Time.time > f_laserTimer + 1.4f + f_howLongWillLaserShoot + 1.2f)
            {
                LaserReturnToPool();
            }
        }

        public void LaserDamageAndHitEffect()
        {
            if (!b_laserDamageCounting ) { return; }

            //if(Physics.Raycast(ray_skyToGround, out hitObject, 40f))
            //{
            //    if (hitObject.collider.gameObject.tag=="Controller")
            //    {
            //        fightSystem.currentHp -= 10;
            //    }
            //}

            //用方形射線判斷造成的傷害，因為天降雷射很粗
            if (Physics.BoxCast(co_laserCollider.bounds.center, co_laserCollider.transform.localScale, co_laserCollider.transform.up, out hitObject, Quaternion.identity))
            {
                if (hitObject.collider.gameObject.tag == "Controller")
                {
                    //fightSystem.currentHp -= 10f;
                    skyLaserHitEffectPool.ReUse(hitObject.point, Quaternion.identity);
                }
                else if (hitObject.collider.gameObject.layer == LayerMask.NameToLayer("Default"))
                {
                    f_hitGroundTimer += Time.deltaTime;
                    if (f_hitGroundTimer > f_hitGroundEffectMake)
                    {
                        laserHitGroundEffectPool.ReUse(hitObject.point + new Vector3(Random.Range(0.0f, 0.2f), 0.1f, 0.5f), Quaternion.identity);
                        f_hitGroundTimer = 0;
                    }
                }
            }
            

        }

        public void LaserReturnToPool()
        {
            if (!gameObject.activeInHierarchy) { return; }
            

            laserPool.Recovery(this.gameObject);

        }
        
        /// <summary>
        /// 裡面有bool直會受到ShootLaser的影響，ShootLaser控制那個Bool讓這個發炫要不要射出collider
        /// </summary>
        public void ShootLaserCollider()
        {            
            if (!b_laserDamageCounting) { return; }
            collider_timer += Time.deltaTime;
            if (collider_timer>f_attackRate)
            {
                laserColliderPool.ReUse(this.transform.position, Quaternion.Euler(90,0,0));
                collider_timer = 0;
            }          
        }

        public void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(go_LaserParant.transform.position, go_LaserParant.transform.position + go_LaserParant.transform.forward);
            Gizmos.color = Color.magenta;
            //Gizmos.DrawLine(go_LaserParant.transform.position, go_LaserParant.transform.position + go_LaserParant.transform.up*40);
            Gizmos.DrawRay(ray_skyToGround);
        }
    }
}