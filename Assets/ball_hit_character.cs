﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball_hit_character : MonoBehaviour
{
    // Start is called before the first frame update
    // public GameObject effext;//撥的特效
    //float rayDistance = 10.0f;//
    //public ParticlePool Pool;//特效物件池
    public GameObject lighting;//砍中裡面讓他一直播
                               // public GameObject other;//砍中的物體(要取它的collider)
                               // private Transform last;//刀子最後出現的位置存取點
    void Start()
    {

        lighting.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision other)//剛打到敵人collider
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Controller"))
        {

            print("ball hit");
            lighting.SetActive(true);
            //   GameObject gameObject=  Pool.ReUse(other.GetContact(0).point,Quaternion.LookRotation(other.GetContact(0).point - this.transform.position, Vector3.up)) ;
            // Pool.Recovery()
            //StartCoroutine(effectPlay(gameObject));
            print("被打中");
            lighting.GetComponentInChildren<ParticleSystem>().loop = true;//啟動粒子特效做迴圈
            lighting.GetComponentInChildren<ParticleSystem>().Play(true);
        }
    }
    private void OnCollisionExit(Collision other)//剛打到敵人collider
    {
        lighting.GetComponentInChildren<ParticleSystem>().loop = false;
        lighting.GetComponentInChildren<ParticleSystem>().Play(false);
    }




}
