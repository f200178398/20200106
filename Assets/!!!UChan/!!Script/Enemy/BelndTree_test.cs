﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BelndTree_test : MonoBehaviour
{
    [Range(0, 1)]
    public float speed;

    public string[] one_attack;
    public string[] two_attack;

    public bool twoHand;
    public bool playAnim;
    Animator anim;

    public bool TwoHand { get => twoHand; set => twoHand = value; }

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    
    void Update()
    {
        if (playAnim)
        {
            string targetAnim;

            if (!twoHand)
            {
                int r = Random.Range(0, one_attack.Length);
                targetAnim =one_attack [r];
            }
            else
            {
                int r = Random.Range(0, two_attack.Length);
                targetAnim = two_attack[r];
                Debug.Log("two");
            }

            speed = 0;
            anim.CrossFade(targetAnim, 0.2f);
            playAnim = false;
        }

        anim.SetFloat("vertical", speed);

    }
}
