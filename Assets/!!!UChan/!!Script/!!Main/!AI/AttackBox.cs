﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class AttackBox : MonoBehaviour
    {
        public Collider attackBox_col;
        public Collider attackBox_col_L;
        public Collider attackBox_col_Foot;

        public GameObject attackBox;

        public EnemyTarget enemyTarget;

        void Start()
        {
            enemyTarget = gameObject.GetComponentInParent<EnemyTarget>();

            if (enemyTarget.b_IsKyle)
            {
                Enemy_Sword_effect.SetActive(false);
            }
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void Close_attackBox()
        {
            attackBox_col.enabled = !enabled;
            
        }
        public void Open_attackBox() //攻擊動畫的Event判定碰撞器的開啟
        {
            attackBox_col.enabled = enabled;
            
        }

        public void Open_attackBox_L() //左手
        {
            attackBox_col_L.enabled = enabled;

        }
        public void Close_attackBox_L()
        {
            attackBox_col_L.enabled = !enabled;

        }

        public void Open_attackBox_Foot() //左手
        {
            attackBox_col_Foot.enabled = enabled;

        }
        public void Close_attackBox_Foot()
        {
            attackBox_col_Foot.enabled = !enabled;

        }


        #region 凱爾刀光
        public GameObject Enemy_Sword_effect;//6 凱爾刀光揮過效果

        void Enemy_Sword_action_func()
        {
            Enemy_Sword_effect.SetActive(true);

            //print("這裡開始");
            // StartCoroutine("WAITTOend");
        }
        void Enemy_Sword_action_func_end()
        {
            Enemy_Sword_effect.SetActive(false);
            //StartCoroutine("WAITTOend");

        }
        #endregion
    }

}
