﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SA;
public class PowerChop : MonoBehaviour
{
    public PowerChopPool pool;
    public float startTime;    
    public float powerChopLife = 1f;
    public float chopSpeed;
    // Start is called before the first frame update
    void Start()
    {
        pool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<PowerChopPool>();
    }

    // Update is called once per frame
    void Update()
    {
        UsePowerChop();
        PowerChopLifeReturnToPool();
    }
    private void OnEnable()
    {
        startTime += Time.time;
    }
    void UsePowerChop()
    {

        //if(attackEvent.isBladeFury == true)
        //{           
        this.transform.position += this.transform.forward * chopSpeed * Time.deltaTime;
        //attackEvent.isBladeFury = false;                          
        //}               
    }
    void PowerChopLifeReturnToPool()
    {
        if (Time.time > startTime + powerChopLife)//如果現在的遊戲時間大於 物件被Active時的時間+泡泡生命時，把泡泡回收
        {
            pool.Recovery(this.gameObject);
        }
    }
   
    
}
