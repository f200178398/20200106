﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SA;
public class UI_TotalDamage : MonoBehaviour
{
    public Text text_damage;
    public float totalDamage;
    public FightSystem fightSystem;
    void Start()
    {
        
        fightSystem = GameObject.Find("FightSystem").GetComponent<FightSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        DisplayDamage();
    }
     public void DisplayDamage()
    {
        totalDamage = fightSystem.totalDamage;
        text_damage.text = totalDamage.ToString();
    }
}
