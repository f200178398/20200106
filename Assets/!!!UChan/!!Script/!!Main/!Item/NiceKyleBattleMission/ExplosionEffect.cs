﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEffect : MonoBehaviour
{
    public ExplosionEffectPool explosionEffectPool;
    public float _timer=0f;
    public float effectLife = 2.5f;
    // Start is called before the first frame update
    void Awake()
    {
        explosionEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ExplosionEffectPool>();        
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    private void OnEnable()
    {
        _timer = Time.time;
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer + effectLife)
        {
            explosionEffectPool.Recovery(gameObject);
        }
    }

}
