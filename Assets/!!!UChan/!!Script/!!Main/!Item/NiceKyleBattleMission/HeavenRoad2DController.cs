﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavenRoad2DController : MonoBehaviour
{
    public GameObject thisCameraObject;
    public Camera heaven2DCamera;
    [Header("攝影機開關的Trigger")]
    public TimelineManager tm_heavenRoadTri;
    public TimelineManager tm_bossAppearedTri;
    [Header("尋找玩家")]
    public GameObject controller;
    
    // Start is called before the first frame update
    void Start()
    {
        thisCameraObject = this.gameObject;
        heaven2DCamera = GetComponent<Camera>();
        heaven2DCamera.enabled = false;
        tm_heavenRoadTri = GameObject.Find("AnimEvent_04_飛船登場_Tri").GetComponent<TimelineManager>();
        tm_bossAppearedTri = GameObject.Find("AnimEvent_05_boss登場Tri").GetComponent<TimelineManager>();
        controller = GameObject.FindGameObjectWithTag("Controller");
    }

    // Update is called once per frame
    void Update()
    {
        CameraSwitch();
    }

    private void FollowPlayerPosition()
    {
        transform.position = Vector3.Slerp(transform.position, new Vector3( transform.position.x
            , transform.position.y, controller.transform.position.z), 1.0f);
    }

    public void CameraSwitch()
    {
        if ( tm_heavenRoadTri.timelineAnimation.state == UnityEngine.Playables.PlayState.Paused && tm_heavenRoadTri.b_hasBeenTiggered
            && !tm_bossAppearedTri.b_hasBeenTiggered )
        {
            FollowPlayerPosition();
            StartCoroutine(OpenCamera());
        }
        else if(tm_bossAppearedTri.b_hasBeenTiggered)
        {            
            StartCoroutine(CloseCamera());
        }
        else
        {
            StartCoroutine(CloseCamera());
        }
    }

    public IEnumerator OpenCamera()
    {        
        heaven2DCamera.enabled = true;
        yield return null;
    }
    public IEnumerator CloseCamera()
    {
        yield return new WaitForSeconds(0.1f);
        heaven2DCamera.enabled = false;
    }
}
