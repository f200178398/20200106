﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{

    [Header("Floating Settings")]
    public float rangeOfArc = 0; //計算用，會一直加
    public float perRangeOfArc = 0.03f; //每秒漂浮變動量
    public float radius = 0.45f;//上下漂浮的範圍
    public GameObject go_Takaramono_Child;
    public TreasurePool treasurePool;

    // Start is called before the first frame update
    void Awake()
    {
        treasurePool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<TreasurePool>();
    }

    // Update is called once per frame
    void Update()
    {
        FloatingMove();
    }

    public void FloatingMove()
    {
        rangeOfArc += perRangeOfArc;
        float dy = Mathf.Cos(rangeOfArc) * radius; //用三角函數讓他那個值在某區間浮動
        go_Takaramono_Child.transform.position= new Vector3(transform.position.x, this.transform.position.y + dy, transform.position.z);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Controller")
        {
            treasurePool.Recovery(gameObject);
        }
    }
}
