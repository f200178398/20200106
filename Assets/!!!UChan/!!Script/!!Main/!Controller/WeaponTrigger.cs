﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class WeaponTrigger : MonoBehaviour
    {
        //****用 來控制武器的Collider開關後與敵人的觸發


        public bool attackTri;
        public FightSystem fightSy;
        public Collider col;
        bool attack;
        public float tempTime;
        public float cd = 1f;

        /// <summary>
        /// 擊中敵人的特效
        /// </summary>
        public GameObject effext;
        float rayDistance = 10.0f;
        public ParticlePool Pool;//特效物件池
        public GameObject lighting;//砍中裡面讓他一直播
        public GameObject other;//砍中的物體(要取它的collider)
        private Transform last;//刀子最後出現的位置

        private void Awake()
        {
            fightSy = GameObject.Find("FightSystem"). GetComponent<FightSystem>();
        }
        private void Start()
        {
            Pool = GameObject.Find("ObjectPool").GetComponent<ParticlePool>();
        }
        private void Update()
        {
            
        }

        private void OnCollisionEnter(Collision other)//剛打到敵人collider
        {
            
        }

        IEnumerator effectPlay(GameObject effect)
        {
            yield return new WaitForSeconds(3);
            Pool.Recovery(effect);
            //Destroy(effect);
        }

        public void OnTriggerEnter(Collider target) //判定武器是否接觸到敵人
        {
            //attackTri = true;
            
            if (target.gameObject.tag == "Enemy"  /*&& StateManager.Instance().rt*/)
            {
                GameObject gameObject = Pool.ReUse(this.transform.position, Quaternion.identity);
                // Pool.Recovery()
                StartCoroutine(effectPlay(gameObject));
                //print("被打中");

                attackTri = true;
                
            }
            else
            {
                attackTri = false;
                
            }

        }
        public void OnTriggerExit(Collider target) // 
        {
            if (target.gameObject.tag == "Enemy")  // 如果目標不是敵人, 則不會觸發武器判定
            {
                //attackObj = null;
                attackTri = false;

            }
        }
    }

}
