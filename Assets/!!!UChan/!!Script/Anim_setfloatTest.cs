﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim_setfloatTest : MonoBehaviour
{


    protected Animator animator;
    public float DirectionDamoTime = 0.25f;
    void Start()
    {
        animator = GetComponent<Animator>();
    }


    void Update()
    {

        if (animator)
        {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.IsName("Base Layer.Run"))//注意这里必须是Base Layer.Run   Base Layer是动画层的名称，用.的形式引出当前准备切换动画的动画状态名称
            {
                if (Input.GetKeyDown(KeyCode.Space))
                    animator.SetBool("Jump", true);
            }
            else
            {
                animator.SetBool("Jump", false);
            }
            //在奔跑时才可以水平旋转
            float horizontal = Input.GetAxis("Horizontal");
            animator.SetFloat("Direction", horizontal, DirectionDamoTime, Time.deltaTime);

            //控制idle到跑状态的切换
            if (Input.GetKeyDown(KeyCode.W))
            {
                animator.SetFloat("Blend", 1.5f);
                
            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                animator.SetFloat("Blend", 0);
            }
        }
    }
}
