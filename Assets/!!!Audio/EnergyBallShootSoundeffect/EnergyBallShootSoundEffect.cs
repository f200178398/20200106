﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBallShootSoundEffect : MonoBehaviour
{
    AudioSource as_shootEffectSound;
    void Awake()
    {
        as_shootEffectSound = GetComponent<AudioSource>();
    }

    void OnEnable()
    {
        as_shootEffectSound.Play();
    }
}
