﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplosionSoundEffect : MonoBehaviour
{
    public BombExplosionSoundEffectPool bombExplosionSoundEffectPool;
    [Header("Sound Settings")]
    public AudioSource as_explosionSound;
    public int index_explosionSoundType=0;
    public AudioClip[] ac_explosionSoundArray=null;
    [Header("Time Settings")]
    public float _timer=0;
    public float lifeTime=3f;
    
    // Start is called before the first frame update
    void Awake()
    {
        bombExplosionSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BombExplosionSoundEffectPool>();
        as_explosionSound = GetComponent<AudioSource>();
        index_explosionSoundType = Random.Range(0, ac_explosionSoundArray.Length);

    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    private void OnEnable()
    {
        _timer = Time.time;
        as_explosionSound.clip = ac_explosionSoundArray[index_explosionSoundType];
        as_explosionSound.Play();        
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if ( Time.time > _timer +lifeTime )
        {
            index_explosionSoundType = Random.Range(0, ac_explosionSoundArray.Length);
            bombExplosionSoundEffectPool.Recovery(gameObject);
        }
    }
}
