﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMachineFireOnSoundEffect : MonoBehaviour
{

    public FollowMachineFireOnSoundEffectPool followMachineFireOnSoundEffectPool;
    public float _timer;
    public float effectLife;
    public AudioSource as_fireOnSound;

    // Start is called before the first frame update
    void Awake()
    {
        followMachineFireOnSoundEffectPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<FollowMachineFireOnSoundEffectPool>();
        as_fireOnSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }
    private void OnEnable()
    {
        as_fireOnSound.Play();
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer + effectLife)
        {
            //as_fireOnSound.Stop();
            followMachineFireOnSoundEffectPool.Recovery(gameObject);
        }
    }
}
