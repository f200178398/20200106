﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleHitPlayerSound : MonoBehaviour
{
    public BubbleHitPlayerSoundPool bubbleHitPlayerSoundPool;
    public float _timer;
    public float effectLife = 1.5f;
    public AudioSource as_BubbleHitSound;
    // Start is called before the first frame update
    void Awake()
    {
        as_BubbleHitSound = GetComponent<AudioSource>();
        bubbleHitPlayerSoundPool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<BubbleHitPlayerSoundPool>();
    }

    // Update is called once per frame
    void Update()
    {
        ReturnToPool();
    }

    private void OnEnable()
    {
        _timer = Time.time;
        as_BubbleHitSound.Play();
    }

    public void ReturnToPool()
    {
        if (!gameObject.activeInHierarchy) { return; }
        if (Time.time > _timer + effectLife)
        {
            as_BubbleHitSound.Stop();
            bubbleHitPlayerSoundPool.Recovery(gameObject);
        }
    }
}
