﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FollowMachine
{
    public class FollowMachine_2 : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void FixedUpdate()
        {
            InFollowState();
            Shoot();
        }
        //跟隨時漂浮、轉向
        [Header("Floating Settings")]
        public float rangeOfArc = 0; //計算用，會一直加
        public float perRangeOfArc = 0.03f; //每秒漂浮變動量
        public float radius = 0.45f;//上下漂浮的範圍

        //跟隨
        [Header("Following Settings")]
        public GameObject player; //跟隨的目標
        private Vector3 tempVec = Vector3.zero; //輔助用向量
                                                /*下面三個調整機器與主角之間的距離*/
        public float followXPoint = -0.72f;
        public float followYPoint = 2f;
        public float followZPoint = -0.50f;

        public void InFollowState()
        {
            rangeOfArc += perRangeOfArc;//一直增加的值
            float dy = Mathf.Cos(rangeOfArc) * radius; //用三角函數讓他那個值在某區間浮動

            //跟隨目標的向量再加一個位置
            Vector3 temp = new Vector3(followXPoint, dy + followYPoint, followZPoint) + player.transform.position;
            // y座標是會變動的，所以跟隨的位置也會變動
            //柔軟的跟著目標(跟隨相機)
            transform.position = Vector3.SmoothDamp(transform.position, temp, ref tempVec, 0.5f);
            //如果非攻擊狀態，同player的rotation
            onAttacking = false;
            FollowAngleMode();
        }


        [Header("Other Settings")]
        public bool onAttacking = false; //是否攻擊狀態

        protected void FollowAngleMode()
        {
            if (onAttacking == false)//false的時候抓player的rotation
            {
                this.transform.rotation = player.transform.rotation;
            }
            else//true的時候瞄準攝影機中央的球
            {
                Vector3 targetAngle = ballAtCameraCenter.position - this.transform.position;
                this.transform.rotation = transform.rotation = Quaternion.LookRotation(targetAngle);
            }

        }








        //射擊
        [Header("Shooting Settings")]
        public GameObject machineBullet; //子彈
        public Transform machineEmitter; //子彈發射口
        public Transform ballAtCameraCenter; //一直掛在相機中間的
                                             /*下面這三個影響到射擊時與主角的位置*/
        public float shootPositionX = 0.0f;
        public float shootPositionY = 2.0f;
        public float shootPositionZ = -0.5f;

        public void MakeBullet()
        {
            GameObject MachineBulletClone = Instantiate
                (machineBullet,
                machineEmitter.transform.position,
                machineEmitter.transform.rotation) as GameObject;
        }
        public void Shoot()
        {
            if (Input.GetKey(KeyCode.P))
            {
                onAttacking = true;
                Vector3 shootPosition = player.transform.position + new Vector3(shootPositionX, shootPositionY, shootPositionZ);//這邊這個new向量影響漂浮機攻擊時的位置與主角之間的位置
                transform.position = shootPosition;
                //即使是順移到位置，射擊時也還是會有超小幅度的上下位移

                //射擊時，瞄準那顆球
                FollowAngleMode();

                //生出子彈
                MakeBullet();
            }

        }
    }
}