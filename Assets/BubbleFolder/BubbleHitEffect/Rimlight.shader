// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:True,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:33232,y:32728,varname:node_9361,prsc:2|emission-378-OUT;n:type:ShaderForge.SFN_Tex2d,id:9419,x:32590,y:32773,ptovrint:False,ptlb:base_texture,ptin:_base_texture,varname:node_9419,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9d2e18603ddc2874885e12d4df43daf0,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Fresnel,id:6580,x:32522,y:32951,varname:node_6580,prsc:2|EXP-4991-OUT;n:type:ShaderForge.SFN_Multiply,id:861,x:32716,y:33030,varname:node_861,prsc:2|A-6580-OUT,B-3914-RGB;n:type:ShaderForge.SFN_Color,id:3914,x:32427,y:33149,ptovrint:False,ptlb:rim_color,ptin:_rim_color,varname:node_3914,prsc:2,glob:False,taghide:False,taghdr:True,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6320754,c2:0.4382787,c3:0.4382787,c4:1;n:type:ShaderForge.SFN_Slider,id:794,x:31990,y:32952,ptovrint:False,ptlb:rim_strength,ptin:_rim_strength,varname:node_794,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2820513,max:1;n:type:ShaderForge.SFN_RemapRange,id:4991,x:32347,y:32951,varname:node_4991,prsc:2,frmn:0,frmx:1,tomn:11,tomx:0.1|IN-794-OUT;n:type:ShaderForge.SFN_Color,id:2373,x:32548,y:32578,ptovrint:False,ptlb:base_color,ptin:_base_color,varname:node_2373,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:3069,x:32891,y:32738,varname:node_3069,prsc:2|A-2373-RGB,B-9419-RGB;n:type:ShaderForge.SFN_Add,id:378,x:32989,y:32958,varname:node_378,prsc:2|A-3069-OUT,B-861-OUT;proporder:2373-9419-3914-794;pass:END;sub:END;*/

Shader "Rimlight" {
    Properties {
        _base_color ("base_color", Color) = (0.5,0.5,0.5,1)
        _base_texture ("base_texture", 2D) = "white" {}
        [HDR]_rim_color ("rim_color", Color) = (0.6320754,0.4382787,0.4382787,1)
        _rim_strength ("rim_strength", Range(0, 1)) = 0.2820513
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma target 3.0
            uniform sampler2D _base_texture; uniform float4 _base_texture_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float4, _rim_color)
                UNITY_DEFINE_INSTANCED_PROP( float, _rim_strength)
                UNITY_DEFINE_INSTANCED_PROP( float4, _base_color)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _base_color_var = UNITY_ACCESS_INSTANCED_PROP( Props, _base_color );
                float4 _base_texture_var = tex2D(_base_texture,TRANSFORM_TEX(i.uv0, _base_texture));
                float _rim_strength_var = UNITY_ACCESS_INSTANCED_PROP( Props, _rim_strength );
                float4 _rim_color_var = UNITY_ACCESS_INSTANCED_PROP( Props, _rim_color );
                float3 emissive = ((_base_color_var.rgb*_base_texture_var.rgb)+(pow(1.0-max(0,dot(normalDirection, viewDirection)),(_rim_strength_var*-10.9+11.0))*_rim_color_var.rgb));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
